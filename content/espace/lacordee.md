---
nom: La Cordée
logo: https://www.la-cordee.net/wp-content/uploads/CORDEE_LOGO.png.webp
photo: https://www.la-cordee.net/wp-content/uploads/Liberte%CC%81-1-845x321-300x114.jpeg
---

<!-- logo -->
![La Cordée](https://www.la-cordee.net/wp-content/uploads/CORDEE_LOGO.png.webp)
<!-- sous-titre -->
La Cordée c'est dix espaces de coworking dans toute la France.
10 espaces de travail & de partage !
> 🇬🇧 Ten collaborative Working Spaces in France

# La Cordée

<!-- Photo -->
![](https://www.la-cordee.net/wp-content/uploads/Liberte%CC%81-1-845x321-300x114.jpeg)

<!-- Présentation: 1000 caractères max avec les services -->
La Cordée, ce sont des espaces de travail professionnels, conviviaux et lumineux, où l’on croise des personnes exerçant tous les métiers : consultants, entrepreneurs, commerciaux, développeurs ou encore traducteurs. Ce sont également des espaces d’échange, où l’on peut demander conseil à notre voisin avocat ou trouver un graphiste pour une prestation. Et ce sont enfin des espaces de convivialité, où l’on peut prendre un café en discutant de ses projets et déjeuner tous ensemble le midi autour d’une grande table.



## Services
- Internet Fibre par câble ethernet ou wifi
- Salle de réunion
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- Café à volonté ☕
- Thé dispo en vrac 🌿
- Terrasse l'été pour certains de nos espaces ☀️

## Localisation

Créée en 2011, La Cordée compte aujourd'hui 10 espaces de coworking situés à Lyon, Nantes, Rennes Paris et Annecy.
Toutes les informations et les photos de nos espaces sont sur notre site :

https://www.la-cordee.net/
Pour nous contacter : https://www.la-cordee.net/
