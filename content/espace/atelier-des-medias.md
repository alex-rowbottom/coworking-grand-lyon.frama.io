---
nom: L’Atelier des Médias
logo: https://pbs.twimg.com/profile_images/1294516507/logo_sans_signature_400x400.jpg
photo: https://i.imgur.com/aQKOHZl.jpg
---

<!-- logo -->
![Atelier](/images/atelier-des-medias-logo.svg)
<!-- sous-titre -->
Coworking associatif imaginé par et pour les professionnels indépendants des médias, du web, de l'image...
> 🇬🇧: An associative coworking, created by and for independent professionals of media, web, images ...

# L’Atelier des Médias

<!-- Photo -->
![](/images/atelier-des-medias-photo.jpeg)

<!-- Présentation: 1000 caractères max avec les services -->
Créé en 2011, l'Atelier des Médias fonctionne sur un esprit collaboratif pour que les indépendants développent ensemble de nouvelles solidarités professionnelles.

Installée en centre ville dans un lieu chaleureux de plus de 200m², l'association est autogérée grâce à un système de groupes de travail auquel participe chaque coworker, ce qui permet de créer des liens, à la fois personnels et professionnels.

## Services
- 5 salles de travail en open space
- 1 salle réservée aux entretiens téléphoniques
- 1 salle de pause qui fait aussi office de salle des fêtes
- Internet en wifi
- 3 lignes téléphoniques fixes
- 1 imprimante/scanner en libre service
- accès illimité à la machine à café, et plein de thés aussi
- 1 cuisine avec un grand frigo et 3 fours à micro-ondes
- des casiers fermés
- 1 jeu de fléchettes
- 1 pompe pour les pneus de ton vélo

## Localisation

L'Atelier des Médias est situé sur le quai du Rhône, entre l'opéra et le tunnel de la Croix Rousse. À quelques minutes de la place des Terreaux, de la rue de la République et des pentes de la Croix Rousse.

[9 quai André Lassagne - 69001 Lyon](https://www.openstreetmap.org/relation/1297608#map=19/45.77117/4.83755)

https://www.atelier-medias.org/

inscriptions@atelier-medias.org
