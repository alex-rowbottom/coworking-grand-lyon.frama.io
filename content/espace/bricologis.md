---
nom : Bricologis
---

<!-- logo -->
![Bricologis](https://www.net1901.org/image/use2/7/77799.jpg)
<!-- sous-titre -->
*Espace ressource pour faire soi même et faire ensemble*

# Bricologis

<!-- Photo -->
![](https://cdn-s-www.leprogres.fr/images/15D95910-EE28-4D0D-9306-CD9D7EA9DA61/NW_raw/pelagie-lea-theo-yana-et-sebastien-la-nouvelle-equipe-de-bricologis-photo-progres-monique-desgouttes-rouby-1619093360.jpg)
<!-- Présentation: 1000 caractères max avec les services -->
Bricologis est une association qui à comme objet d’être un espace ressource pour monter en compétences et en pouvoir d’agir (faire soi-même) à travers le lien social (faire ensemble).

Tu es artisan.e , association, individu ou collectif sans structure à la recherche d’un espace pour ton activité ? Tu souhaites évoluer dans une « ambiance co-working » mais qui propose un fonctionnement social et solidaire ? Dans un quartier populaire et dynamique ?

Viens nous rencontrer et découvrir nos locaux de 200 mètres carrés avec des espaces privatifs de 9 à 16m2, un atelier de bricolage, 2 salles de réunions et tous les équipements nécessaires. Pas de loyer mais une contribution aux charges et au projet de l’association seront demandés ! Ton projet répond soit à un besoin du territoire, soit permet un lien avec l’activité de Bricologis. A nous d’imaginer nos synergies !

Les ressources proposées à nos adhérents :
1.	Un atelier de bricolage accompagné et un dispositif de prêt d’outils.
2.	Des espaces à disposition de porteurs de projets
3.	Un espace de vie sociale ouvert à tous
4.	Un accompagnement  de projets pour les habitants, individus ou collectifs avec ou sans structure juridique

## Services
- 7 bureaux de 11 à 16 m2
- 1 salle de réunion de 20 m2
- Salle à manger et cuisine partagée
- Atelier bois de 90m2 avec stock et salle de finition
- Internet, reprographie et consommables mutualisés
- Partage de réseaux, partenariats et compétences
- Matériel de réparation cycle & pied d'atelier 🚲
- Café et thé vrac à volonté ☕

##  Localisation
Nous sommes situés au 6 chemin du Grand Bois à Vaulx en Velin, en plein coeur du Mas du Taureau. Parking voiture à priximité, accessible avec le bus C3 arrêt Lesire, ou en vélo (nous disposons d'un local à vélo privé).Très proche de la sortie nord de l'autoroute, pratique pour éviter les bouchons !

[6 Chemin du Grand Bois, 69 120 Vaulx en Velin](https://goo.gl/maps/bonSpowGF9hHTafF6)
[https://www.bricologis.com/](https://www.bricologis.com/)
07 69 02 52 90
contact@bricologis.com
